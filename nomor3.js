const arr = [43, 6, 6, 5, 54, 81, 71, 56, 8, 877, 4, 4];

const isPrima = n => {
   if (n===1){
      return false;
   }else if(n === 2){
      return true;
   }else{
      for(let x = 2; x < n; x++){
         if(n % x === 0){
            return false;
         }
      }
      return true;
   };
};

const jumlahPrima = arr => {
   let sum = 0;
   for(let i = 0; i < arr.length; i++){
      if(!isPrima(arr[i])){
         continue;
      };
      sum += arr[i];
   };
   return sum;
};
console.log(jumlahPrima(arr));